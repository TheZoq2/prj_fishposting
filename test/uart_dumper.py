#top = main::uart_dumper
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, FallingEdge
from spade import SpadeExt
import cocotb

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i

    await cocotb.start(Clock(clk, 2, units = 'ns').start())

    s.i.channels = "[1,2,3,4,5,6,7,8]"
    s.i.bit_time = "6"
    s.i.rst = "true"
    await ClockCycles(clk, 3, rising=False)
    s.i.rst = "false"

    await ClockCycles(clk, 8_000, rising=False)

