#top = cppm::cppm_decoder

from cocotb.triggers import FallingEdge
from spade import SpadeExt

import cocotb
from cocotb.clock import Clock

class Config:
    def __init__(self, ms1, ms2, ms3, shift):
        self.ms1 = ms1
        self.ms2 = ms2
        self.ms3 = ms3
        self.shift = shift

    def to_spade(self) -> str:
        return f"""
            Config $(
            ms3: {self.ms3},
            ms2: {self.ms2},
            ms1: {self.ms1},
            scale: 1,
            ms_to_10_bit_shift: {self.shift},
        )
        """

config = Config(100, 200, 300, 0)

async def low_for(clk, s: SpadeExt, duration: int):
    s.i.signal = "false"
    [await FallingEdge(clk) for _ in range(0, duration)]

async def high_for(clk, s: SpadeExt, duration: int):
    s.i.signal = "true"
    [await FallingEdge(clk) for _ in range(0, duration)]

async def channel(clk, s: SpadeExt, value: int):
    await low_for(clk, s, config.ms1 // 2)
    await high_for(clk, s, value + config.ms1 // 2)

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut)

    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units = 'ns').start())

    s.i.config = config.to_spade()

    s.i.signal = "true"
    s.i.rst = "true"
    await FallingEdge(clk);
    s.i.rst = "false"

    # Let it settle
    await high_for(clk, s, 400)

    await channel(clk, s, 50)
    await channel(clk, s, 100)
    await channel(clk, s, 0)
    await channel(clk, s, -5)
    await channel(clk, s, 50)
    await channel(clk, s, 50)
    await channel(clk, s, 50)
    await channel(clk, s, 50)

    await low_for(clk, s, 50)
    await high_for(clk, s, 400)

    s.o.assert_eq("[50, 100, 0, 0, 50, 50, 50, 50]")




