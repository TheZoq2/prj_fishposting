# Fishposting

A DIY submersible based on a raspberry pi zero w and an ecpix5 FPGA on the surface.

The sub is built from PVC pipes, 2 floating pipes at the top, 2 pipes filled
with rocks at the bottom, and one with electronics in the middle. This setup
gives stability so I only need 2 thrusters facing forward, and one for going up
and down.

The thrusters are bilge pumps because they are designed to be submersed and
waterproof.

Inside the sub, there is a raspberry pi which controls the motors and which has
a camera for recording, and ""streaming"" video to the surface. I did that
because it's simple, the pi can have a camera already, can both record and
transmit analog video, and can be re-programemd via wifi, so I don't have to
open it up so often.

The wire between the sub and the surface is an ethernet cable and a tick power
cable. The ethernet cable has one pair of cables for the analog video, and
a UART port for control and telemetry. 

The control and telemetry is handled by some Spade of course. Control is mostly
reading the "trainer port" of my RC radio and sending that over that uart link.
The telemtry data is drawn on a separate sharp memory display, but might be embedded into
the video stream by the FPGA in the future.

## Pi config

This runs on Rasbian 11 

The following `/boot/config.txt` configuration for the pi sets up i2c, uart and
composite video out.
```
# For more options and information see
# http://rpf.io/configtxt
# Some settings may impact device functionality. See link above for details

# uncomment if you get no picture on HDMI for a default "safe" mode
#hdmi_safe=1

# uncomment the following to adjust overscan. Use positive numbers if console
# goes off screen, and negative if there is too much border
#overscan_left=16
#overscan_right=16
#overscan_top=16
#overscan_bottom=16

# uncomment to force a console size. By default it will be display's size minus
# overscan.
#framebuffer_width=1280
#framebuffer_height=720

# uncomment if hdmi display is not detected and composite is being output
#hdmi_force_hotplug=1

# uncomment to force a specific HDMI mode (this will force VGA)
#hdmi_group=1
#hdmi_mode=1

# uncomment to force a HDMI mode rather than DVI. This can make audio work in
# DMT (computer monitor) modes
#hdmi_drive=2

# uncomment to increase signal to HDMI, if you have interference, blanking, or
# no display
#config_hdmi_boost=4


#uncomment to overclock the arm. 700 MHz is the default.
#arm_freq=800

# Uncomment some or all of these to enable the optional hardware interfaces
dtparam=i2c_arm=on
#dtparam=i2s=on
dtparam=spi=on

# Uncomment this to enable infrared communication.
#dtoverlay=gpio-ir,gpio_pin=17
#dtoverlay=gpio-ir-tx,gpio_pin=18

# Additional overlays and parameters are documented /boot/overlays/README

# Enable audio (loads snd_bcm2835)
dtparam=audio=on

# Automatically load overlays for detected cameras
start_x=1

# Automatically load overlays for detected DSI displays
display_auto_detect=1

# Enable DRM VC4 V3D driver
#dtoverlay=vc4-kms-v3d,composite
max_framebuffers=2

# Disable compensation for displays with overscan
disable_overscan=1

[cm4]
# Enable host mode on the 2711 built-in XHCI USB controller.
# This line should be removed if the legacy DWC2 controller is required
# (e.g. for USB device mode) or if USB support is not required.
otg_mode=1

[all]

[pi4]
dtoverlay=vc4-fkms-v3d
# Run as fast as firmware / board allows
arm_boost=1

[all]
gpu_mem=128
enable_uart=1
dtoverlay=disable-bt

sdtv_mode=2
hdmi_ignore_hotplug=1
```
