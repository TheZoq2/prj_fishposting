module top
    ( input clk
    , input cppm_in
    , output[2:0] ld5
    , output[2:0] ld6
    , output[2:0] ld7
    , output[2:0] ld8
    , output uart_tx
    , input uart_rx
    , output uart_tx_pc
    , output disp_clk
    , output disp_di
    , output disp_cs
    );

    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end

    assign uart_tx_pc = uart_tx;

    \prj_fishposting::main::main main
        ( .clk_i(clk)
        , .rst_i(rst)
        , .output__({ld5, ld6, ld7, ld8})
        , .tx(uart_tx)
        , .rx(uart_rx)
        , .cppm_in_raw(cppm_in)
        , .disp_clk(disp_clk)
        , .disp_di(disp_di)
        , .disp_cs(disp_cs)
        );
endmodule
