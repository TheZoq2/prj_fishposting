use memory_display::main::OutputPins;
use memory_display::main::Output;
use memory_display::main::DisplayGenOutput;
use memory_display::main::Timing;
use memory_display::main::display_gen;
use memory_display::main::output_translator;

use lib::char_rom::char_rom_sprite_rom;

use std::mem::clocked_memory;
use std::mem::read_memory;
use std::ports::new_mut_wire;
use std::ports::read_mut_wire;

struct Cursor {
    x: int<9>,
    y: int<9>
}


struct port TextRomWritePort {
    addr: &mut int<10>,
    val: &mut Option<int<7>>
}

impl TextRomWritePort {
    entity fill_with_a(self, clk: clock, rst: bool) -> bool {
        reg(clk) addr reset(rst: 0) = trunc(addr + 1);

        set self.addr = addr;
        set self.val = Some(65u);

        true
    }

    entity display_text(
        self,
        clk: clock,
        rst: bool,
        next_char: Option<int<8>>
    ) -> bool {
        reg(clk) Cursor$(x, y) reset(rst: Cursor(0, 0)) = {
            match next_char {
                Some(3) => {
                    Cursor(0, 0)
                },
                Some(10) => {
                    if y == 20 {
                        Cursor(0, 0)
                    } else {
                        Cursor(0, trunc(y + 1))
                    }
                },
                Some(_) => {
                    if x == 50 {
                        Cursor(x, y)
                    } else {
                        Cursor(trunc(x + 1), y)
                    }
                },
                None => Cursor$(x, y)
            }
        };

        let write = match next_char {
            Some(3) => None(),
            Some(10) => None(),
            Some(other) => Some(trunc(other)),
            None => None()
        };

        let x: int<16> = zext(x);
        let y: int<16> = y * 50;
        set self.addr = trunc(x + y);
        set self.val = write;

        true
    }
}

struct port TextRomReadPort {
    addr: &mut int<10>,
    out: &int<7>
}


pipeline(1) read_text_rom(clk: clock, p: TextRomReadPort, addr: int<10>) -> int<7> {
        set p.addr = addr;
    reg;
        *p.out
}


pipeline(1) text_rom(clk: clock) -> (TextRomReadPort, TextRomWritePort) {
        let write_port = TextRomWritePort(inst new_mut_wire(), inst new_mut_wire());
        let read_port = TextRomReadPort(inst new_mut_wire(), &stage(+1).out);

        let mem: Memory<int<7>, 950> = inst clocked_memory(
            clk,
            match inst read_mut_wire(write_port.val) {
                Some(val) => [(true, inst read_mut_wire(write_port.addr), val)],
                None => [(false, 0, 0)]
            }
        );
        let out = inst read_memory(mem, inst read_mut_wire(read_port.addr));
    reg;
        (read_port, write_port)
}

struct TextDrawerState {
    major: (int<9>, int<9>),
    minor: (int<3>, int<4>)
}

pipeline(3) text_drawer(clk: clock, rst: bool, display_signals: DisplayGenOutput, text_rom: TextRomReadPort) -> int<4> {
        let new_line = match display_signals.o {
            Output::Pixel((0, _)) => true,
            _ => false
        };
        let new_frame = match display_signals.o {
            Output::Pixel((0, 0)) => true,
            _ => false
        };

        let (pixel, (x, y)) = match display_signals.o {
            Output::Pixel((x, y)) => (true, (x, y)),
            _ => (false, (0, 0))
        };

        reg(clk) (old_x, old_y) = {
            if pixel {
                (x, y)
            }
            else {
                (old_x, old_y)
            }
        };

        let tick = pixel && (old_x != x || old_y != y);

        reg(clk) state reset (rst: TextDrawerState((0, 0), (0, 0))) = {
            match (tick, new_frame, new_line) {
                (false, _, _) => state,
                // New frame => clear everything
                (_, true, _) => TextDrawerState$(major: (0, 0), minor: (0, 0)),
                // New line => increment line and reset x
                (_, _, true) => {
                    let TextDrawerState$(major: (maj_x, maj_y), minor: (min_x, min_y)) = state;

                    if min_y == 12u {
                        TextDrawerState$(major: (0, trunc(maj_y + 1)), minor: (0, 0))
                    }
                    else {
                        TextDrawerState$(major: (0, maj_y), minor: (0, trunc(min_y + 1)))
                    }
                },
                // Otherwise => increment x, keep y 
                (_, _, _) => {
                    let TextDrawerState$(major: (maj_x, maj_y), minor: (min_x, min_y)) = state;

                    if min_x == 7u {
                        TextDrawerState$(major: (trunc(maj_x + 1), maj_y), minor: (0, min_y))
                    }
                    else {
                        TextDrawerState$(major: (maj_x, maj_y), minor: (trunc(min_x + 1), trunc(min_y)))
                    }
                }
            }
        };
    reg;
        let TextDrawerState$(major: (major_x, major_y), minor: (minor_x, minor_y)) = stage(-1).state;

        let major_x: int<16> = zext(major_x);
        let major_y: int<16> = major_y * 50;

        let index = inst(1)read_text_rom(clk, text_rom, trunc(major_x + major_y));
    reg;
        let (color, _) = inst(1) char_rom_sprite_rom(clk, index, minor_x, minor_y, 0, 0, 0);
    reg;
        color
}

pipeline(3) run_display(clk: clock, rst: bool, text_rom: TextRomReadPort) -> OutputPins {
        // Create a timing struct and provide the durations of the fields at your target
        // frequency
        let t = Timing$(
            us1: 100,
            us3: 300,
            mhz1: 100
        );

        // Instantiate the `display_gen` entity
        let display_signals = inst display_gen(clk, rst, t);

        // Translate the pixel coordinates of the output into boolean pixel values. This
        // example generates a checkerboard pattern with an offset

        let (x, y) = match display_signals.o {
            Output::Pixel((x, y)) => (x, y),
            _ => (0, 0)
        };
        let color = inst(3) text_drawer(clk, rst, display_signals, text_rom);
    reg*3;
        let with_pixels = match display_signals.o {
            Output::UpdateMode => Output::UpdateMode(),
            Output::FrameInv(v) => Output::FrameInv(v),
            Output::AllClear => Output::AllClear(),
            Output::Dummy => Output::Dummy(),
            Output::Address(v) => Output::Address(v),
            Output::Pixel((x, y)) => {
                Output::Pixel(color != 0)
            },
            Output::CsHigh => Output::CsHigh(),
            Output::CsLow => Output::CsLow(),
        };

        // Generate the spi clk, mosi and chip select signals by calling the output translator function
        output_translator(with_pixels, display_signals.sclk)
}

