from typing import Any, List, Optional
from pathlib import Path

from serial.serialposix import Serial

import gpiozero
from gpiozero import LED
from time import sleep
import serial
import datetime
import os
from picamera import PiCamera

import board
import busio
import adafruit_bno08x
from adafruit_bno08x.i2c import BNO08X_I2C
from adafruit_bno08x import (
    BNO_REPORT_ACCELEROMETER,
    BNO_REPORT_GYROSCOPE,
    BNO_REPORT_MAGNETOMETER,
    BNO_REPORT_ROTATION_VECTOR,
)

class Channels:
    def __init__(self, inner: List[int]):
        self.inner = inner

    def channel_float_centered(self, idx: int) -> float:
        return (self.inner[idx] - 512) / 512

    def channel_float_full(self, idx: int) -> float:
        return self.inner[idx] / 1024

    def up_down(self) -> Optional[float]:
        if self.armed():
            return -self.channel_float_centered(2)
        else:
            return None

    def left(self) -> Optional[float]:
        if self.armed():
            return max(0, min(self.channel_float_centered(3) + self.channel_float_full(0), 1.0))
        else:
            return None

    def right(self) -> Optional[float]:
        if self.armed():
            return max(0, min(-self.channel_float_centered(3) + self.channel_float_full(0), 1.0))
        else:
            return None

    def shutdown(self) -> bool:
        return self.inner[5] > 768

    def armed(self) -> bool:
        return self.inner[4] < 512

    def take_picture(self) -> bool:
        return self.inner[1] > 512

    def record(self) -> bool:
        return self.inner[6] > 512

    def print(self):
        print(f"u/d: {self.up_down()}; arm: {self.armed()}")


class Motor:
    def __init__(self, pin1: int, pin2: int, invert: bool):
        self.invert = invert
        self.motor = gpiozero.Motor(pin1, pin2)
        self.motor.stop()

    def stop(self):
        self.motor.stop()

    def run(self, dir: Optional[float]):
        if dir is None:
            self.stop()
        else:
            if self.invert:
                dir = -dir

            if abs(dir) < 0.3:
                self.stop()
            elif dir < 0:
                self.motor.backward(-dir)
            else:
                self.motor.forward(dir)


def find_next_filename(dir: Path, base: str, extension: str) -> Path:
    i = 0
    while True:
        file = (dir / (f"{base}_{i:04}.{extension}"))
        if not file.exists():
            return file
        i += 1

def write_line(ser: Serial, line: str):
    ser.write(f"{line:<50}\n".encode('utf-8'))

def reset_cursor(ser: serial.Serial):
    ser.write((3).to_bytes(1, 'big'))

def report_heading(ser: Serial, bno: Optional[Any]):
    if bno:
        try:
            mag_x, mag_y, mag_z = bno.magnetic
            write_line(ser, f"Heading: {mag_z:.0f}")
        except:
            write_line(ser, "Heading read error")
    else:
        write_line(ser, "Heading sensor not detected")

def main():
    camera = PiCamera()
    camera.hflip = True
    camera.vflip = True
    camera.start_preview()
    # camera.start_recording("test.h264")

    took_picture = False
    recording = False


    print("Initializing motion sensor")
    try:
        i2c = busio.I2C(board.SCL, board.SDA, frequency=800000)
        bno = BNO08X_I2C(i2c)
        bno.enable_feature(BNO_REPORT_MAGNETOMETER)
    except Exception as e:
        print(e)
        bno = None

    try:
        with serial.Serial('/dev/ttyAMA0', 115200, timeout=0) as ser:

            print("Serial opened", flush=True)
            print("Writing to serial", flush=True)

            for _ in range(0, 20):
                ser.write(f"{'':<50}\n".encode("utf-8"))
            reset_cursor(ser)

            queue = b""
            last_channels = None

            left_motor = Motor(23, 24, False)
            right_motor = Motor(17, 27, True)
            up_down_motor = Motor(5, 6, False)

            shutdown_start = None

            last_valid = datetime.datetime.now()

            while True:
                x = ser.read(128)
                queue += x

                if b'\n' in queue:
                    [line, *rest] = queue.split(b'\n')
                    queue = b"".join(rest)
                    segments = line.split(b' ')

                    try:
                        channels_raw = list(map(lambda x: int(x, 16), segments))

                        valid = len(channels_raw) == 8 and abs(channels_raw[7] - 512) < 10

                        if valid:
                            last_channels = Channels(channels_raw)
                            last_valid = datetime.datetime.now()

                        if (datetime.datetime.now() - last_valid).seconds > 1:
                            last_channels = None
                    except ValueError as e:
                        print(f"Failed to decode channel (string: {line}) {e}")
                        continue

                if last_channels:
                    last_channels.print()
                    up_down_motor.run(last_channels.up_down())
                    left_motor.run(last_channels.left())
                    right_motor.run(last_channels.right())

                    print(last_channels.inner)

                    if last_channels.shutdown():
                        if shutdown_start is None:
                            shutdown_start = datetime.datetime.now()

                        print(f"Shutting down soon. Time left {10 - (datetime.datetime.now() - shutdown_start).seconds}")

                        if (datetime.datetime.now() - shutdown_start).seconds > 10:
                            os.system("sudo shutdown now")
                            exit()
                    else:
                        shutdown_start = None

                    if last_channels.take_picture() and not took_picture:
                        print("Taking picture")
                        camera.capture(find_next_filename(Path("Pictures"), "PIC", "jpg").__bytes__())
                    took_picture = last_channels.take_picture()

                    if last_channels.record() and not recording:
                        print("Starting recording")
                        recording = True
                        camera.start_recording(find_next_filename(Path("Pictures"), "VID", "h624").__bytes__(), format="h264")
                    if recording and not last_channels.record():
                        print("Stopping recording")
                        recording = False
                        camera.stop_recording()

                    if last_channels.armed():
                        status_msg = f"Armed {last_channels.up_down():.2f} {last_channels.left():.2f} {last_channels.right():.2f}. Rec {recording}"
                    else:
                        status_msg = f"Disarmed Rec {recording}"

                    write_line(ser, "")
                    write_line(ser, status_msg)
                    report_heading(ser, bno)
                    reset_cursor(ser)



                else:
                    up_down_motor.stop()
                    write_line(ser, "Controller not seen yet")
                    reset_cursor(ser)
    except KeyboardInterrupt:
        camera.stop_recording()




print("Starting up", flush=True)

main()


